import numpy as np
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import sklearn
import sklearn.datasets

df=pd.read_csv('housing.data',delim_whitespace=True,header=None)

col_name = ['CRIM','ZN','INDUS','CHAS','NOX','RM','AGE','DIS','RAD','TAX','PTRATIO','B','LSTAT','MEDV']
df.columns = col_name
print(df.describe())

sns.pairplot(df, height=1.5)
plt.show()

col_study=['CRIM','ZN','INDUS','NOX','RM']
sns.pairplot(df[col_study], size=2.5)
plt.show()

col_study2=['PTRATIO','B','LSTAT','MEDV']
sns.pairplot(df[col_study2], size=2.5)
plt.show()

print(df.corr())

plt.figure(figsize=(16,16))
sns.heatmap(df.corr(),annot=True)
plt.show()

plt.figure(figsize=(16,16))
sns.heatmap(df[['CRIM','ZN','INDUS','CHAS','MEDV']].corr(),annot=True)
plt.show()

X = df['RM'].values.reshape(-1,1)
y = df['MEDV'].values

from sklearn.linear_model import LinearRegression

# Prva znacajka: RM
x = df['RM'].values.reshape(-1, 1)
y = df['MEDV']
lm1 = LinearRegression()
lm1.fit(X, y)

# Druga znacajka: LSTAT
x = df['LSTAT'].values.reshape(-1, 1)
y = df['MEDV']
lm2 = LinearRegression()
lm2.fit(X, y)

# Treca znacajka: PTRATIO
x = df['PTRATIO'].values.reshape(-1, 1)
y = df['MEDV']
lm3 = LinearRegression()
lm3.fit(X, y)

model = LinearRegression()
model.fit(X,y)
print(model.coef_)
print(model.intercept_) 

plt.figure(figsize=(12,10))
sns.regplot(X,y)
plt.xlabel('Prosjecan broj soba po stanu')
plt.ylabel('Srednja vrijednost kuće u kojima žive vlasnici [ X 1000 dolara]')
plt.show()

sns.jointplot(x='RM',y='MEDV', data=df, kind='reg',size=10)
plt.show()

# predikcija

print(lm1.predict(np.array([[5], [8]])))
print(lm2.predict(np.array([[5], [8]])))
print(lm3.predict(np.array([[5], [8]])))
