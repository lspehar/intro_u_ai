import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn
import sklearn.datasets

df = pd.read_csv('auto-mpg.data-original', sep = '\s+', header = None)
df = df.dropna()

col_names = ['mpg', 'cylinders', 'displacement', 'horsepower', 'weight', 'acceleration', 'model_year', 'origin', 'car_name']
df.columns = col_names
print(df.describe())

print(df.corr())

plt.figure(figsize=(16, 16))
sns.heatmap(df.corr(), annot=True)
plt.show()

from sklearn.linear_model import LinearRegression

# 1. Horsepower
X = df['horsepower'].values.reshape(-1, 1)
y = df['mpg']
lm1 = LinearRegression()
lm1.fit(X, y)

# 2. Weight
X = df['weight'].values.reshape(-1, 1)
y = df['mpg']
lm2 = LinearRegression()
lm2.fit(X, y)

# 3. Accel.
X = df['acceleration'].values.reshape(-1, 1)
y = df['mpg']
lm3 = LinearRegression()
lm3.fit(X, y)