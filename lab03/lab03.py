import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')
import pandas as pd
import sklearn
import sklearn.datasets
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression

df = pd.read_csv('data.csv', sep = ',', header = [1])
df = df.dropna()

col_name = [ 'id','diagnosis','radius_mean','texture_mean','perimeter_mean','area_mean','smoothness_mean','compactness_mean','concavity_mean','concave points_mean','symmetry_mean','fractal_dimension_mean','radius_se','texture_se','perimeter_se','area_se','smoothness_se','compactness_se','concavity_se','concave points_se','symmetry_se','fractal_dimension_se','radius_worst','texture_worst','perimeter_worst','area_worst','smoothness_worst','compactness_worst','concavity_worst','concave points_worst','symmetry_worst','fractal_dimension_worst' ]

df.columns = col_name
df = df.drop('id', axis = 1)
df = df.dropna()
df['diagnosis'] = df['diagnosis'].replace({'B':1, 'M':0 })
print(df.describe())
print(df.corr())

corr_values = df.corr()['diagnosis'].abs()
sorted_corr = corr_values.sort_values(ascending = False)
print(sorted_corr[1:3])

plt.figure(figsize = (16, 16))
sns.heatmap(df.corr(), annot = True)
plt.show()

X = df.drop('diagnosis', axis = 1)
y = df['diagnosis']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 42)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

clf = LogisticRegression(random_state = 42)
clf.fit(X_train, y_train)
scores = cross_val_score(clf, X_test, y_test, cv = 2)
print(f'\nCross-validation scores: {scores}\nAverage scores: {np.mean(scores)}')