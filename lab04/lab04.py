import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression

# ucitaj dataset:
iris = load_iris()
X = iris.data[:, 2:]
y = iris.target

# mapiraj klase u brojeve
y = np.where(y == 0, 0, np.where(y == 1, 1, 2))

# podjela na skup za treniranje i testiranje:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.4, random_state = 42)

# skaliranje:
sc_x = StandardScaler()
X_std_train = sc_x.fit_transform(X_train)

# kreiraj k-NN klasifikator:
knn = KNeighborsClassifier(n_neighbors = 20, metric = 'minkowski')

# treniraj knn:
knn.fit(X_std_train, y_train)

# krizna validacija knn:
y_test_pred_knn = cross_val_predict(knn, sc_x.transform(X_test), y_test, cv = 7)

# razultati KNN:
print("K-NN:\n", confusion_matrix(y_test, y_test_pred_knn))
print("Preciznost: ", precision_score(y_test, y_test_pred_knn, average = 'weighted'))
print("Odziv: ", recall_score(y_test, y_test_pred_knn, average = 'weighted'))
print("F1 mjera: ", f1_score(y_test, y_test_pred_knn, average = 'weighted'))



# SVM klasifikator:
svm = SVC(kernel = 'linear', C = 1, random_state = 42)

# treniraj SVM:
svm.fit(X_std_train, y_train)

# krizna validacija SVM:
y_test_pred_svm = cross_val_predict(svm, sc_x.transform(X_test), y_test, cv = 7)

# rezultati SVM:
print("\nSVM:\n", confusion_matrix(y_test, y_test_pred_svm))
print("Preciznost: ", precision_score(y_test, y_test_pred_svm, average = 'weighted'))
print("Odziv: ", recall_score(y_test, y_test_pred_svm, average = 'weighted'))
print("F1 mjera: ", f1_score(y_test, y_test_pred_svm, average = 'weighted'))



# klasifikator log regresije:
logreg = LogisticRegression(solver = 'lbfgs', multi_class = 'multinomial', max_iter = 1000, random_state = 42)

# treniraj:
logreg.fit(X_std_train, y_train)

# krizna validacija za log reg:
y_test_pred_logreg = cross_val_predict(logreg, sc_x.transform(X_test), y_test, cv = 7)

# rezultati log reg:
print("\nLogisticka regresija:\n", confusion_matrix(y_test, y_test_pred_logreg))
print("Preciznost: ", precision_score(y_test, y_test_pred_logreg, average = 'weighted'))
print("Odziv: ", recall_score(y_test, y_test_pred_logreg, average = 'weighted'))
print("F1 mjera: ", f1_score(y_test, y_test_pred_logreg, average = 'weighted'))