from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions
import pandas as pd

# Generiraj 700 uzoraka podataka s 5 različitih nakupina
X, y = make_blobs(n_samples = 700, centers = 5, random_state = 42)

n_clusters = 9  # Unaprijed očekivani broj klastera
kmeans = KMeans(n_clusters = 9)
kmeans.fit(X)

# Prepoznavanje klastera
y_kmeans = kmeans.predict(X)

# Koordinate centroida
centers = kmeans.cluster_centers_

# Prikazivanje rezultata
plt.scatter(X[:, 0], X[:, 1], c=y_kmeans)
plt.scatter(centers[:, 0], centers[:, 1], marker='x', s = 200, c = 'red')
plt.xlabel("X1")
plt.ylabel("X2")
plt.title("K-means algoritam")
plt.show()
# Prikazivanje granica odluke
plot_decision_regions(X, y, clf = kmeans)
plt.xlabel("X1")
plt.ylabel("X2")
plt.title("Granice odluke K-means algoritma")
plt.show()

# Pronalaženje optimalnog broja klastera
sse_ = []
for k in range(1, 8):
    kmeans = KMeans(n_clusters = k).fit(X)
    sse_.append([k, kmeans.inertia_])

# Prikazivanje rezultata
plt.plot(pd.DataFrame(sse_)[0], pd.DataFrame(sse_)[1])
plt.xlabel('Broj klastera')
plt.ylabel('SSE')
plt.title('Metoda "lakta"')
plt.show()