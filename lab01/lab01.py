import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

names = ['mpg', 'cylinders', 'displacement', 'horsepower', 'weight', 'acceleration', 'model_year', 'origin', 'car_name']

data = pd.read_csv('auto-mpg.data-original', sep = '\s+', header = None, names = names)
data = data.dropna()
data_new = data.copy()
cols = ['horsepower', 'displacement']
subset = data[cols]
scaler = MinMaxScaler()
scaled = pd.DataFrame(scaler.fit_transform(subset), columns = cols)

fig, (ax1, ax2) = plt.subplots(ncols = 2, figsize = (10, 5))
ax1.set_title('Before')
subset.plot(kind = 'box', ax = ax1)
ax2.set_title('After')
scaled.plot(kind = 'box', ax = ax2)

'''
colors = ['blue' if g < 75 else 'red' for g in data['model_year']]

plt.scatter(data['horsepower'], data['weight'], c = colors)

plt.title('Horsepower (HP) & Weight (KG)')
plt.xlabel('HP')
plt.ylabel('KG')

# show graph:
plt.show()
'''